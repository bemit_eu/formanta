#!/usr/bin/env php
<?php

$build = require __DIR__ . '/lib/build.php';
/**
 * @var \FormantaBlocks\Runner $runner
 */
$runner = $build(null);